package com.solva.aurum.controllers;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.solva.aurum.domain.dto.LimitDto;
import com.solva.aurum.services.LimitService;

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

@ExtendWith(MockitoExtension.class)
class LimitControllerTest {

    @Mock
    private LimitService limitService;

    @InjectMocks
    private LimitController limitController;

    private MockMvc mockMvc;

    private ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    void setUp() {
        objectMapper.registerModule(new JavaTimeModule());
        mockMvc = MockMvcBuilders.standaloneSetup(limitController).build();
    }

    @Test
    void createLimit_shouldReturnCreatedStatusAndSavedLimit() throws Exception {
        LimitDto expectedLimit = createTestLimit();
        when(limitService.createLimit(any(LimitDto.class))).thenReturn(expectedLimit);

        mockMvc.perform(post("/limits")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(expectedLimit)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(expectedLimit.getId())))
                .andExpect(jsonPath("$.limitSum", is(expectedLimit.getLimitSum().intValue())))
                .andExpect(jsonPath("$.limitDatetime", is(expectedLimit.getLimitDatetime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ssX")).toString())))
                .andExpect(jsonPath("$.limitCurrencyShortname", is(expectedLimit.getLimitCurrencyShortname())))
                .andExpect(jsonPath("$.expenseCategory", is(expectedLimit.getExpenseCategory())));
    }

    @Test
    void getAllLimits_shouldReturnListOfLimits() throws Exception {
        List<LimitDto> expectedLimits = List.of(createTestLimit());
        when(limitService.getAllLimits(any(Long.class))).thenReturn(expectedLimits);

        mockMvc.perform(get("/limits")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(Map.of("accountId", 1L))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()", is(1))); // Ensure the returned list contains one element
    }

    private LimitDto createTestLimit() {
        return LimitDto.builder()
                .id(1)
                .accountId(BigDecimal.valueOf(100))
                .limitSum(BigDecimal.valueOf(500))
                .limitDatetime(OffsetDateTime.parse(OffsetDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ssX")), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ssX")).withOffsetSameInstant(ZoneOffset.ofHours(6)))
                .limitCurrencyShortname("KZT")
                .expenseCategory("service")
                .build();
    }
}
