package com.solva.aurum.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.solva.aurum.domain.dto.ExceededTransactionDto;
import com.solva.aurum.domain.dto.TransactionDto;
import com.solva.aurum.services.TransactionService;


@ExtendWith(MockitoExtension.class)
public class TransactionControllerTest {

    @Mock
    private TransactionService transactionService;

    @InjectMocks
    private TransactionController transactionController;

    @Test
    public void testCreateTransaction() throws Exception {
        // Given
        TransactionDto transactionDto = new TransactionDto(1, 123L, 456L, "USD", BigDecimal.TEN, "Food", OffsetDateTime.now(), false);
        when(transactionService.createTransaction(any(TransactionDto.class))).thenReturn(transactionDto);

        // When
        ResponseEntity<TransactionDto> responseEntity = transactionController.createTransaction(transactionDto);

        // Then
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(transactionDto, responseEntity.getBody());
    }

    @Test
    public void testGetTransactionsExceededLimit() {
        // Given
        Long accountId = 123L;
        List<ExceededTransactionDto> exceededTransactionDtos = new ArrayList<>();
        ExceededTransactionDto exceededTransactionDto = new ExceededTransactionDto(123L, 456L, "USD", BigDecimal.TEN, "Food", OffsetDateTime.now(), BigDecimal.ONE, OffsetDateTime.now(), "USD");
        exceededTransactionDtos.add(exceededTransactionDto);
        when(transactionService.getTransactionsExceededLimit(accountId)).thenReturn(exceededTransactionDtos);

        // When
        Map<String, Long> requestBody = new HashMap<>();
        requestBody.put("accountId", accountId);
        ResponseEntity<List<ExceededTransactionDto>> responseEntity = transactionController.getTransactionsExceededLimit(requestBody);

        // Then
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(exceededTransactionDtos, responseEntity.getBody());
    }
}


