package com.solva.aurum.domain.dto;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ExceededTransactionDto {
    private Long accountFrom;

    private Long accountTo;

    private String currencyShortname;

    private BigDecimal sum;

    private String expenseCategory;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ssX", timezone = "Asia/Almaty")
    private OffsetDateTime dateTime;

    private BigDecimal limitSum;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ssX", timezone = "Asia/Almaty")
    private OffsetDateTime limitDatetime;

    private String limitCurrencyShortname;
}
