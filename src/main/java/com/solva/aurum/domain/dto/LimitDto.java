package com.solva.aurum.domain.dto;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LimitDto {
    private Integer id;

    private BigDecimal accountId;

    private BigDecimal limitSum;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ssX", timezone = "Asia/Almaty")
    private OffsetDateTime limitDatetime;

    private String limitCurrencyShortname;

    private String expenseCategory;
    
}
