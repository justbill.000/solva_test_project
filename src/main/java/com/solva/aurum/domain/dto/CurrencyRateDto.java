package com.solva.aurum.domain.dto;

import java.math.BigDecimal;
import java.time.LocalDate;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.solva.aurum.domain.util.CurrencyRateId;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CurrencyRateDto {
    // private String currencyPair;

    // @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Almaty")
    // private LocalDate date;
    private CurrencyRateId id;

    private BigDecimal close;
}
