package com.solva.aurum.domain.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Meta {
    String symbol;
    String interval;
    String currency_base;
    String currency_quote;
    String type;
}
