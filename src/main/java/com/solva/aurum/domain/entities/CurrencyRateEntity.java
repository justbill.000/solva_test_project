package com.solva.aurum.domain.entities;



import jakarta.persistence.Column;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.solva.aurum.domain.util.CurrencyRateId;



@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name="currency_rates")
public class CurrencyRateEntity {
    @EmbeddedId
    private CurrencyRateId id;

    // @Column(length = 7, nullable = false)
    // private String currencyPair;

    // @Column(nullable = false)
    // @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Almaty")
    // private LocalDate date;

    @Column(precision = 10, scale = 4, nullable = false)
    private BigDecimal close;
}
