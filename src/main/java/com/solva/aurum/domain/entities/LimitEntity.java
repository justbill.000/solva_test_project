package com.solva.aurum.domain.entities;



import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.OffsetDateTime;


import com.fasterxml.jackson.annotation.JsonFormat;



@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name="limits")
public class LimitEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "account_id", nullable = false)
    private Long accountId;

    @Column(name="limit_sum", precision = 10, scale = 2, nullable = false)
    private BigDecimal limitSum;

    @Column(name="limit_datetime", nullable = false, columnDefinition = "TIMESTAMPTZ")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ssX", timezone = "Asia/Almaty")
    @Builder.Default
    private OffsetDateTime limitDatetime = OffsetDateTime.now();

    @Column(name="limit_currency_shortname", length = 3, nullable = false)
    private String limitCurrencyShortname;

    @Column(name="expense_category", length = 255, nullable = false)
    private String expenseCategory;
}
