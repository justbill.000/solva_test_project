package com.solva.aurum.domain.entities;



import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.OffsetDateTime;


import com.fasterxml.jackson.annotation.JsonFormat;


@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="transactions")
public class TransactionEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "account_from", nullable = false)
    private Long accountFrom;

    @Column(name = "account_to", nullable = false)
    private Long accountTo;

    @Column(name = "currency_shortname", length = 3, nullable = false)
    private String currencyShortname;

    @Column(name = "sum", precision = 10, scale = 2, nullable = false)
    private BigDecimal sum;

    @Column(name = "expense_category", length = 255, nullable = false)
    private String expenseCategory;

    @Column(name = "datetime", nullable = false)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ssX", timezone = "Asia/Almaty")
    private OffsetDateTime dateTime;

    @Column(name="limit_exceeded", nullable = false)
    @Builder.Default
    private boolean limitExceeded = false;
}
