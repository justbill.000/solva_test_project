package com.solva.aurum;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

import com.solva.aurum.services.impl.TwelveDataService;

import lombok.extern.slf4j.Slf4j;

import java.util.TimeZone;

@Slf4j
@SpringBootApplication
@EnableCaching
public class AurumApplication implements CommandLineRunner{
	@Autowired
	private TwelveDataService twelveDataService;
	
	public static void main(String[] args) {
		TimeZone.setDefault(TimeZone.getTimeZone("Asia/Almaty"));
		SpringApplication.run(AurumApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
	}
}
