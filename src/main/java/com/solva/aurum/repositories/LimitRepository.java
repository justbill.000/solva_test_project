package com.solva.aurum.repositories;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.solva.aurum.domain.entities.LimitEntity;

import jakarta.persistence.Tuple;

@Repository
public interface LimitRepository extends JpaRepository<LimitEntity, Integer>{
    @Query(nativeQuery = true, value = "SELECT id, limit_sum FROM limits WHERE account_id = :accountId AND expense_category = :expenseCategory AND limit_datetime BETWEEN :startDate AND :endDate ORDER BY limit_datetime DESC LIMIT 2")
    Optional<List<Tuple>> findFirstLimitSumByAccountIdAndExpenseCategoryAndLimitDatetimeBetween(
        @Param("accountId") Long accountId,
        @Param("expenseCategory") String expenseCategory,
        @Param("startDate") OffsetDateTime startDate,
        @Param("endDate") OffsetDateTime endDate
    );

    List<LimitEntity> findByAccountId(Long accountId);
}
