package com.solva.aurum.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.solva.aurum.domain.entities.CurrencyRateEntity;
import com.solva.aurum.domain.util.CurrencyRateId;

import jakarta.persistence.Tuple;

@Repository
public interface CurrencyRateRepository extends JpaRepository<CurrencyRateEntity, CurrencyRateId>{
    @Query(nativeQuery = true, value = "SELECT cr.close, cr.date FROM currency_rates cr WHERE cr.currency_pair = :currencyPair ORDER BY cr.date DESC LIMIT 1")
    Optional<Tuple> findMostRecentCloseAndDateByCurrencyPair(@Param("currencyPair") String currencyPair);
}
