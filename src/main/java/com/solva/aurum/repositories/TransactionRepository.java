package com.solva.aurum.repositories;

import java.time.OffsetDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.solva.aurum.domain.entities.TransactionEntity;

@Repository
public interface TransactionRepository extends JpaRepository<TransactionEntity, Integer>{
    List<TransactionEntity> findByAccountFromAndExpenseCategoryAndDateTimeBetween(
            Long accountFrom,
            String expenseCategory,
            OffsetDateTime startDate,
            OffsetDateTime endDate
    );

    @Query(nativeQuery = true, value = "SELECT\r\n" + 
                                        "    t.account_from,\r\n" + 
                                        "    t.account_to,\r\n" + 
                                        "    t.currency_shortname,\r\n" + 
                                        "    t.sum,\r\n" + 
                                        "    t.expense_category,\r\n" + 
                                        "    t.datetime AT TIME ZONE 'Asia/Almaty' as datetime,\r\n" + 
                                        "    l.limit_sum,\r\n" + 
                                        "    l.limit_datetime AT TIME ZONE 'Asia/Almaty' as limit_datetime,\r\n" + 
                                        "    l.limit_currency_shortname\r\n" + 
                                        "FROM transactions t\r\n" + 
                                        "JOIN limits l ON l.account_id = t.account_from\r\n" + 
                                        "WHERE t.limit_exceeded = true\r\n" + 
                                        "AND l.limit_datetime = (\r\n" + 
                                        "    SELECT MAX(limit_datetime)\r\n" + 
                                        "    FROM limits\r\n" + 
                                        "    WHERE account_id = :accountFrom\r\n" + 
                                        "    AND limit_datetime <= t.datetime\r\n" + 
                                        ")\r\n" + 
                                        "AND t.account_from = :accountFrom\r\n" +
                                        "GROUP BY t.id, l.id;")
    List<Object[]> findExceededTransactionsWithLimitsByAccountFrom(@Param("accountFrom") Long accountFrom);
}
