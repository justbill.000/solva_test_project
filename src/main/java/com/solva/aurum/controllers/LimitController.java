package com.solva.aurum.controllers;

import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.solva.aurum.domain.dto.LimitDto;
import com.solva.aurum.services.LimitService;

import io.swagger.v3.oas.annotations.Operation;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;





@RestController
@RequestMapping("/limits")
public class LimitController {

    private LimitService limitService;

    public LimitController(LimitService limitService){
        this.limitService = limitService;
    }

    @Operation(
        description = "Создаёт новый лимит"
    )
    @PostMapping()
    public ResponseEntity<LimitDto> createLimit(@RequestBody LimitDto limitDto) throws JsonProcessingException{
        LimitDto savedLimitEntity = limitService.createLimit(limitDto);

        return ResponseEntity.status(HttpStatus.CREATED).body(savedLimitEntity);
    }

    @Operation(
        description = "Возвращает все лимиты для данного аккаунта. Необходимо указать accountId"
    )
    @GetMapping("")
    public ResponseEntity<List<LimitDto>> getAllLimits(@RequestBody Map<String, Long> requestBody) {
        List<LimitDto> limits = limitService.getAllLimits(requestBody.get("accountId"));
        return ResponseEntity.ok(limits);
    }
}
