package com.solva.aurum.controllers;

import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.solva.aurum.domain.dto.ExceededTransactionDto;
import com.solva.aurum.domain.dto.TransactionDto;
import com.solva.aurum.services.TransactionService;

import io.swagger.v3.oas.annotations.Operation;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;


@RestController
@RequestMapping("/transactions")
public class TransactionController {

    private TransactionService transactionService;

    public TransactionController(TransactionService transactionService){
        this.transactionService = transactionService;
    }

    @Operation(
        description = "Создаёт новую транзакцию (интеграция с банком). Не нужно указывать флаг limitExceeded, автоматически выставлется системой."
    )
    @PostMapping()
    public ResponseEntity<TransactionDto> createTransaction(@RequestBody TransactionDto transactionDto) throws JsonProcessingException{
        TransactionDto savedTransactionEntity = transactionService.createTransaction(transactionDto);

        return ResponseEntity.status(HttpStatus.CREATED).body(savedTransactionEntity);
    }


    @Operation(
        description = "Возвращает все транзакции, превысившие лимит. Нужно указать accountId."
    )
    @GetMapping("/exceeded")
    public ResponseEntity<List<ExceededTransactionDto>> getTransactionsExceededLimit(@RequestBody Map<String, Long> requestBody) {
        List<ExceededTransactionDto> transactions = transactionService.getTransactionsExceededLimit(requestBody.get("accountId"));
        return ResponseEntity.ok(transactions);
    }
}
