package com.solva.aurum.services.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class TwelveDataService {
    private static final String API_URL = "https://api.twelvedata.com/time_series?symbol={symbol}&interval={interval}&apikey={apiKey}";

    private static String API_KEY = "e0913e75b8134904a442b403af9d378a";

    private static final String INTERVAL = "1day";

    private final RestTemplate restTemplate;

    public TwelveDataService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public String getExchangeRate(String symbol) {
        Map<String, String> params = new HashMap<>();
        params.put("symbol", symbol);
        params.put("interval", INTERVAL);
        params.put("apiKey", API_KEY);

        ResponseEntity<String> response = restTemplate.getForEntity(API_URL, String.class, params);

        if (response.getStatusCode() == HttpStatus.OK) {
            return response.getBody();
        } else {
            throw new RuntimeException("Failed to get exchange rate: " + response.getStatusCode());
        }
    }
}