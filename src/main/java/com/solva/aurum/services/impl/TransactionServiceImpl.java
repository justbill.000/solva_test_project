package com.solva.aurum.services.impl;

import java.util.List;
import java.util.ArrayList;
import java.util.Optional;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.sql.Timestamp;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.modelmapper.ModelMapper;
import jakarta.persistence.Tuple;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.solva.aurum.domain.dto.ExceededTransactionDto;
import com.solva.aurum.domain.dto.TransactionDto;
import com.solva.aurum.domain.entities.LimitEntity;
import com.solva.aurum.domain.entities.TransactionEntity;
import com.solva.aurum.repositories.LimitRepository;
import com.solva.aurum.repositories.TransactionRepository;
import com.solva.aurum.services.CurrencyRateService;
import com.solva.aurum.services.TransactionService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class TransactionServiceImpl implements TransactionService{
    private TransactionRepository transactionRepository;

    private LimitRepository limitRepository;

    private CurrencyRateService currencyRateService;

    private ModelMapper modelMapper;

    private RedisTemplate<Integer, BigDecimal> redisTemplate;


    public TransactionServiceImpl(
                                TransactionRepository transactionRepository, 
                                LimitRepository limitRepository, 
                                CurrencyRateService currencyRateService,
                                ModelMapper modelMapper,
                                RedisTemplate<Integer, BigDecimal> redisTemplate
                                ){
        this.transactionRepository = transactionRepository;
        this.limitRepository = limitRepository;
        this.currencyRateService = currencyRateService;
        this.modelMapper = modelMapper;
        this.redisTemplate = redisTemplate;
    }

    @Override
    public TransactionDto createTransaction(TransactionDto transactionDto) throws JsonProcessingException {
        TransactionEntity transaction = modelMapper.map(transactionDto, TransactionEntity.class);

        transaction.setDateTime(transaction.getDateTime().withOffsetSameInstant(ZoneOffset.ofHours(6)));

        transaction.setLimitExceeded(getLimitExceededFlag(transaction.getAccountFrom(), 
                                                        transaction.getSum(), 
                                                        transaction.getExpenseCategory(), 
                                                        transaction.getDateTime(), 
                                                        transaction.getCurrencyShortname()));

        TransactionEntity savedTransaction = transactionRepository.save(transaction);

        return modelMapper.map(savedTransaction, TransactionDto.class);
    }


    private Boolean getLimitExceededFlag(Long accountId, BigDecimal currentTransactionSum, String expenseCategory, OffsetDateTime transactionDateTime, String transactionCurrencyShortname) throws JsonMappingException, JsonProcessingException{
        if(!transactionCurrencyShortname.equals("USD")){
            currentTransactionSum = currencyRateService.convertToUSD(currentTransactionSum, transactionCurrencyShortname, transactionDateTime.toLocalDate());
        }

        BigDecimal lastRemainingLimitSum;

        Integer lastLimitId;
        BigDecimal lastLimitSum;

        Integer preLastLimitId;
        BigDecimal preLastLimitSum;

        OffsetDateTime firstDayOfMonth = transactionDateTime.withDayOfMonth(1).withHour(0).withMinute(0).withSecond(0).withNano(0);
        OffsetDateTime lastDayOfMonth = transactionDateTime.withDayOfMonth(1).plusMonths(1).minusDays(1).withHour(23).withMinute(59).withSecond(59).withNano(999_999_999);

        Optional<List<Tuple>> twoLastMonthLimitsSumAndId = limitRepository.findFirstLimitSumByAccountIdAndExpenseCategoryAndLimitDatetimeBetween(accountId, expenseCategory, firstDayOfMonth, lastDayOfMonth);
        
        if(twoLastMonthLimitsSumAndId.get().isEmpty()){
            LimitEntity limitToSaveEntity = LimitEntity.builder()
                                                                .accountId(accountId)
                                                                .limitSum(new BigDecimal(1000))
                                                                .limitDatetime(firstDayOfMonth)
                                                                .limitCurrencyShortname("USD")
                                                                .expenseCategory(expenseCategory)
                                                                .build();
            lastLimitId = limitRepository.save(limitToSaveEntity).getId();
            lastRemainingLimitSum = new BigDecimal("1000.00");
        } else {
            lastLimitId = twoLastMonthLimitsSumAndId.get().get(0).get(0, Integer.class);
            lastLimitSum = twoLastMonthLimitsSumAndId.get().get(0).get(1, BigDecimal.class);
            log.info(lastLimitSum.toString());

            lastRemainingLimitSum = redisTemplate.opsForValue().get(lastLimitId);

            if(lastRemainingLimitSum == null){
                if(twoLastMonthLimitsSumAndId.get().size() > 1){
                    preLastLimitId = twoLastMonthLimitsSumAndId.get().get(1).get(0, Integer.class);
                    preLastLimitSum = twoLastMonthLimitsSumAndId.get().get(1).get(1, BigDecimal.class);
    
                    lastRemainingLimitSum = redisTemplate.opsForValue().get(preLastLimitId);
                    lastRemainingLimitSum = lastLimitSum.subtract(preLastLimitSum).add(lastRemainingLimitSum);
                } else{
                    lastRemainingLimitSum = lastLimitSum;
                }
            }
        }

        redisTemplate.opsForValue().set(lastLimitId, lastRemainingLimitSum.subtract(currentTransactionSum));

        log.info("currentRemainingLimitSum " + lastRemainingLimitSum.subtract(currentTransactionSum).toString());
        return lastRemainingLimitSum.compareTo(currentTransactionSum) < 0;
    }

    // private Boolean getLimitExceededFlag(Long accountId, BigDecimal currentTransactionSum, String expenseCategory, OffsetDateTime transactionDateTime){
    //     BigDecimal limitSum;

    //     OffsetDateTime firstDayOfMonth = transactionDateTime.withDayOfMonth(1).withHour(0).withMinute(0).withSecond(0).withNano(0);
    //     OffsetDateTime lastDayOfMonth = transactionDateTime.withDayOfMonth(1).plusMonths(1).minusDays(1).withHour(23).withMinute(59).withSecond(59).withNano(999_999_999);

    //     log.info("transactionDateTime " + transactionDateTime.toString());
    //     log.info("firstDayOfMonth " + firstDayOfMonth.toString());

    //     Optional<BigDecimal> lastMonthLimitSum = limitRepository.findFirstLimitSumByAccountIdAndExpenseCategoryAndLimitDatetimeBetween(accountId, expenseCategory, firstDayOfMonth, lastDayOfMonth);
    //     List<TransactionEntity> monthTransactionEntities = transactionRepository.findByAccountFromAndExpenseCategoryAndDateTimeBetween(accountId, expenseCategory, firstDayOfMonth, lastDayOfMonth);
        
    //     if(!lastMonthLimitSum.isPresent()){
    //         LimitEntity limitToSaveEntity = LimitEntity.builder()
    //                                                             .accountId(accountId)
    //                                                             .limitSum(new BigDecimal(1000))
    //                                                             .limitDatetime(firstDayOfMonth)
    //                                                             .limitCurrencyShortname("USD")
    //                                                             .expenseCategory(expenseCategory)
    //                                                             .build();
    //         limitRepository.save(limitToSaveEntity);
    //         limitSum = new BigDecimal("1000.00");
    //     } else {
    //         limitSum = lastMonthLimitSum.get();
    //     }

    //     BigDecimal totalTransactionSum = monthTransactionEntities.stream()
    //                                                                     .map(TransactionEntity::getSum)
    //                                                                     .reduce(BigDecimal.ZERO, BigDecimal::add);
    //     totalTransactionSum = totalTransactionSum.add(currentTransactionSum);

    //     log.info("limitSum " + limitSum.toString());
    //     log.info("totalTransactionSum " + totalTransactionSum.toString());
    //     return limitSum.compareTo(totalTransactionSum) < 0;
    // }


    @Override
    public List<ExceededTransactionDto> getTransactionsExceededLimit(Long accountId) {
        List<Object[]> transactionsExceededLimit = transactionRepository.findExceededTransactionsWithLimitsByAccountFrom(accountId);

        List<ExceededTransactionDto> transactionDtos = new ArrayList<>();

        for (Object[] row : transactionsExceededLimit) {
            Timestamp tsDateTime = (Timestamp) row[5];
            OffsetDateTime dateTime = tsDateTime.toInstant().atOffset(ZoneOffset.ofHours(6));

            Timestamp tsLimitDatetime = (Timestamp) row[7];
            OffsetDateTime limitDatetime = tsLimitDatetime.toInstant().atOffset(ZoneOffset.ofHours(6));

            ExceededTransactionDto transactionDto = new ExceededTransactionDto(
                (Long) row[0],
                (Long) row[1],
                (String) row[2],
                (BigDecimal) row[3],
                (String) row[4],
                dateTime,
                (BigDecimal) row[6],
                limitDatetime,
                (String) row[8]
            );
            transactionDtos.add(transactionDto);
        }

        return transactionDtos;
    }
}
