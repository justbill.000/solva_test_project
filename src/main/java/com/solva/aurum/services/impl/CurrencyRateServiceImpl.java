package com.solva.aurum.services.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Date;
import java.time.LocalDate;
import java.time.Period;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.solva.aurum.domain.entities.CurrencyRateEntity;
import com.solva.aurum.domain.util.CurrencyRateId;
import com.solva.aurum.domain.util.ResponseData;
import com.solva.aurum.repositories.CurrencyRateRepository;
import com.solva.aurum.services.CurrencyRateService;

import jakarta.persistence.Tuple;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CurrencyRateServiceImpl implements CurrencyRateService{
    private CurrencyRateRepository currencyRateRepository;
    private TwelveDataService twelveDataService;

    public CurrencyRateServiceImpl(CurrencyRateRepository currencyRateRepository, TwelveDataService twelveDataService){
        this.currencyRateRepository = currencyRateRepository;
        this.twelveDataService = twelveDataService;
    }

    @Override
    public BigDecimal convertToUSD(BigDecimal amount, String currency, LocalDate datetime) throws JsonProcessingException{
        BigDecimal mostRecentClose;
        Optional<Tuple> mostRecentCloseAndDate = currencyRateRepository.findMostRecentCloseAndDateByCurrencyPair("USD/" + currency);
        if(!mostRecentCloseAndDate.isPresent()){
            mostRecentClose = updateCurrencyRatesTable(null, currency);
        } else{
            LocalDate mostRecentDate = mostRecentCloseAndDate.get().get(1, Date.class).toLocalDate();
            if(!mostRecentDate.isEqual(datetime)){
                if(Period.between(mostRecentDate, datetime).getDays() > 3){
                    mostRecentClose = updateCurrencyRatesTable(mostRecentDate, currency);
                } else{
                    mostRecentClose = mostRecentCloseAndDate.get().get(0, BigDecimal.class);
                }
            } else{
                mostRecentClose = mostRecentCloseAndDate.get().get(0, BigDecimal.class);
            }
        }
        
        log.info("Most recent close: " + mostRecentClose.toString());
        log.info("converted currency sum " + amount.divide(mostRecentClose, 2, RoundingMode.HALF_UP).toString());
        return amount.divide(mostRecentClose, 2, RoundingMode.HALF_UP);
    }


    public BigDecimal updateCurrencyRatesTable(LocalDate stopDate, String currency) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        ResponseData responseData = mapper.readValue(twelveDataService.getExchangeRate("USD/" + currency), ResponseData.class);

        List<Map<String, String>> values = responseData.getValues();
        BigDecimal firstClose = new BigDecimal(values.get(0).get("close"));

        for (Map<String, String> value : values) {
            LocalDate responseDate = LocalDate.parse(value.get("datetime"));
            if((stopDate != null) &&  (responseDate.isEqual(stopDate))){
                break;
            }

            BigDecimal responseClose = new BigDecimal(value.get("close"));

            currencyRateRepository.save(CurrencyRateEntity.builder()
                                                                    .close(responseClose)
                                                                    .id(CurrencyRateId.builder().currencyPair("USD/" + currency)
                                                                                                .date(responseDate).build())
                                                                    .build());
        }

        return firstClose;
    }
}
