package com.solva.aurum.services.impl;

import java.util.List;
import java.util.stream.Collectors;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.solva.aurum.domain.dto.LimitDto;
import com.solva.aurum.domain.entities.LimitEntity;
import com.solva.aurum.repositories.LimitRepository;
import com.solva.aurum.services.LimitService;
import com.solva.aurum.services.CurrencyRateService;


@Service
public class LimitServiceImpl implements LimitService{
    LimitRepository limitRepository;

    CurrencyRateService currencyRateService;

    private ModelMapper modelMapper;

    public LimitServiceImpl(LimitRepository limitRepository, ModelMapper modelMapper, CurrencyRateService currencyRateService){
        this.limitRepository = limitRepository;
        this.modelMapper = modelMapper;
        this.currencyRateService = currencyRateService;
    }

    @Override
    public LimitDto createLimit(LimitDto limitDto) throws JsonProcessingException {
        LimitEntity limitEntity = modelMapper.map(limitDto, LimitEntity.class);
        limitEntity.setLimitDatetime(OffsetDateTime.now(ZoneOffset.ofHours(6)).truncatedTo(ChronoUnit.SECONDS));

        if(!limitEntity.getLimitCurrencyShortname().equals("USD")){
            limitEntity.setLimitSum(currencyRateService.convertToUSD(limitEntity.getLimitSum(), limitEntity.getLimitCurrencyShortname(), limitEntity.getLimitDatetime().toLocalDate()));
            limitEntity.setLimitCurrencyShortname("USD");
        }
        

        return modelMapper.map(limitRepository.save(limitEntity), LimitDto.class);
    }

    @Override
    public List<LimitDto> getAllLimits(Long accountId) {
        List<LimitEntity> limitEntities = limitRepository.findByAccountId(accountId);
        return limitEntities.stream()
            .map(entity -> modelMapper.map(entity, LimitDto.class))
            .collect(Collectors.toList());
    }   
}
