package com.solva.aurum.services;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.solva.aurum.domain.dto.LimitDto;

public interface LimitService {
    public LimitDto createLimit(LimitDto limitDto) throws JsonProcessingException;
    public List<LimitDto> getAllLimits(Long accountId);
}
