package com.solva.aurum.services;

import java.math.BigDecimal;
import java.time.LocalDate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

public interface CurrencyRateService {
    public BigDecimal convertToUSD(BigDecimal amount, String currency, LocalDate datetime) throws JsonMappingException, JsonProcessingException;
}
