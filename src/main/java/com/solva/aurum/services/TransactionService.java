package com.solva.aurum.services;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.solva.aurum.domain.dto.ExceededTransactionDto;
import com.solva.aurum.domain.dto.TransactionDto;

public interface TransactionService {
    public TransactionDto createTransaction(TransactionDto transactionDto) throws JsonProcessingException;
    public List<ExceededTransactionDto> getTransactionsExceededLimit(Long accountId);
}
