package com.solva.aurum.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.annotations.info.Contact;

@OpenAPIDefinition(
    info = @Info(
        contact = @Contact(
            name = "Miras",
            email = "m_pirniyazov@kbtu.kz"
        ),
        description = "OpenApi documentation for my project",
        title = "OpenApi generated documentation",
        version = "latest"
    ),
    servers = {
        @Server(
            description = "Localhost",
            url = "localhost:8080"
        ),
        @Server(
            description = "Remote public server",
            url = "http://167.71.33.232:8080"
        )
    }
)
public class OpenApiConfig {
    
}
